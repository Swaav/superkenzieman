let timestamp = Date.now()
let errCount = 0


let intervalID = setInterval(
    myCallback, 5000)

const fetchDABODYMUSIC = () => {
    intervalID
}

function myJumpForce() {
    console.log('request made')
    console.log(errCount)
    if (errCount >= 2) {
        clearInterval(intervalID)
        return
    }
    fetch('/latest', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'after':timestamp})
    }).then(res => res.json())
        .then(res => {
            timestamp = res.timestamp
            for(let image of res.images){
                let link = document.createElement('a')
                link.href = '/'+image
                let curImage = document.createElement('img')
                curImage.src = '/uploads/'+image
                link.appendChild(curImage)
                document.body.prepend(link)
            }
        })
        .catch(err => {
            console.log('ERROR')
            errCount++
        })
}

fetchDABODYMUSIC();

