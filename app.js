const express = require("express");
const app = express();
const bodyParser = require('body-parser')
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())
app.set('view engine', 'pug')

var multer = require('multer');
var upload = multer({ dest: 'public/uploads/' });
var type = upload.single('myFile');

var fs = require('fs');

let comments = {}
fs.readFile('./comments.json', (err, data) => {
    comments = JSON.parse(data)
})

app.post('/latest', (req, res) => {
    console.log('request received')
    console.log(req.body)
    const path = './public/uploads'
    // const after = req.body.after
    fs.readdir(path, function (err, items) {
        let images = []
        let timestamp = 0
        
        
        items.forEach(imageName => {
            let imageTime = fs.statSync(path+'/'+imageName).mtime.getTime()
            console.log('imageT:'+imageTime)
            console.log('after:'+req.body.after)
            if(imageTime > req.body.after){
                images.push(imageName)
            }
            if(imageTime > timestamp){
                timestamp = imageTime
            }
        })
        console.log(images)
        res.send({
            images,
            timestamp
        })
        // res.send('something')
    //     // res.render('index', { title: 'KGRAM', items: items });
    });
})

app.post('/upload/', type, (req, res) => {
    res.redirect(`/${req.file.filename}`)
})


app.post('/:filename/', type, (req, res) => {
    if (req.body != {}) {
        if (comments[req.params.filename] === undefined) {
            comments[req.params.filename] = []
            let newComment = {}
            newComment.name = req.body.name
            newComment.comment = req.body.comment
            comments[req.params.filename].push(newComment)
        } else {
            let newComment = {}
            newComment.name = req.body.name
            newComment.comment = req.body.comment
            comments[req.params.filename].push(newComment)
        }
    }
    fs.writeFileSync('comments.json', JSON.stringify(comments))
    res.render('base', {
        picPath: './uploads/' + req.params.filename,
        comments: comments, actionPath: req.params.filename
    })
})

app.get('/:filename/', (req, res) => {
    res.render('base', {
        picPath: './uploads/' + req.params.filename,
        comments: comments, actionPath: req.params.filename
    })
})

app.get('/', (req, res) => {
    const path = './public/uploads';

    fs.readdir(path, function (err, items) {
        res.render('index', { title: 'KGRAM', items: items });
    });
})


const port = 3000;

app.listen(port, () => console.log(`Server started on port ${port}`));